angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('menu.recolecciones', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/recolecciones.html',
        controller: 'recoleccionesCtrl'
      }
    }
  })

  .state('welcome', {
    url: '/page6',
    templateUrl: 'templates/welcome.html',
    controller: 'welcomeCtrl'
  })

  .state('iniciarSesiN', {
    url: '/page8',
    templateUrl: 'templates/iniciarSesiN.html',
    controller: 'iniciarSesiNCtrl'
  })

  .state('registrarse', {
    url: '/page7',
    templateUrl: 'templates/registrarse.html',
    controller: 'registrarseCtrl'
  })

  .state('menu.historial', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/historial.html',
        controller: 'historialCtrl'
      }
    }
  })

  .state('menu.miCuenta', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/miCuenta.html',
        controller: 'miCuentaCtrl'
      }
    }
  })

  .state('menu.ayuda', {
    url: '/page5',
    views: {
      'side-menu21': {
        templateUrl: 'templates/ayuda.html',
        controller: 'ayudaCtrl'
      }
    }
  })

  .state('menu.acerca', {
    url: '/page3',
    views: {
      'side-menu21': {
        templateUrl: 'templates/acerca.html',
        controller: 'acercaCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true
  })

$urlRouterProvider.otherwise('/side-menu21/page1')

  

});